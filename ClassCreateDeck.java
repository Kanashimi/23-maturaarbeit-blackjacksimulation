package BlackjackSimulation;

import java.util.ArrayList;

public class ClassCreateDeck {
	// Instanced variables
	int numd;
	private int cardsinthedeck = 52;

	// Constructor
	ClassCreateDeck() {
	}// Constructor

	public ArrayList<Integer> createdeck(int numd) {

		System.out.println("ClassCreateDeck - creating carddeck...");
		// -> Variables
		// what number the added card will have
		int cardnumber = 11;
		// counts how many cards of a type have been added
		int cardcounter = 4;

		// new List
		ArrayList<Integer> carddeck = new ArrayList<Integer>();
		// adds the cards to the list
		while (cardnumber > 0) {
			cardcounter = 4;
			while (cardcounter > 0) {
				carddeck.add(cardnumber);
				cardcounter--;
			}
			if (cardnumber == 10) {
				int tens = 12;
				while (tens > 0) {
					carddeck.add(cardnumber);
					tens--;
				}
			}
			cardnumber--;
		}
		System.out.println("ClassCreateDeck - carddeck is " + carddeck);
		return carddeck;
	}

	public int getcard(ArrayList<Integer> cards) {
		//get random number
		System.out.println("ClassCreateDeck - getting random card... ");
		ClassRandomNumber rannum = new ClassRandomNumber();
		double cardindeck = 52;
		int rcard = cards.get(rannum.randomnumber(cardindeck));
		System.out.println("ClassCreateDeck - random card is = " + rcard);
		return rcard;
	}
	
	public int getcardsinthedeck(){
		System.out.println("ClassCreateDeck - providing <cardsinthedeck> which has the value = " + cardsinthedeck);
		return cardsinthedeck;
	}
	
}//Class
