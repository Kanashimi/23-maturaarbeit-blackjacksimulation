package BlackjackSimulation;

import java.util.Scanner;

public class ClassGetUserInput {
	//Instance variables
	int num;
	
	// Constructor
	public ClassGetUserInput() {
	}//Constructor
	
	public int userinput() {
		System.out.println("ClassGetUserInput - getting userinput...");
		Scanner sc= new Scanner(System.in);
		System.out.print("-> INPUT-> How many decks do you want? - ");
		int num = sc.nextInt();
		sc.close();
		System.out.println("ClassGetUserInput - userinput is = " + num);
		return num;
	}

}//Class
