package BlackjackSimulation;

public class ClassRandomNumber {

	// Constructor
	public ClassRandomNumber() {	
	}//Constructor

	//Generates a random number between 0 and the max cards in the deck
	public  int randomnumber(double cardindeck) {
		System.out.println("ClassRandomNumber - generating random number...");
		int min = 0;
		double rnumber = Math.random();
		int randomNumber = (int) (rnumber * cardindeck - min) + min;
		cardindeck--;
		System.out.println("ClassRandomNumber - returning random number with the value = " + randomNumber);
		return randomNumber;
	}

}//Class