package BlackjackSimulation;

import java.util.ArrayList;

//Overview
//in progress - Launcher
//DONE - ClassCheckCards
//DONE - ClassCreateDeck
//DONE - ClassGetUserInput
//DONE - ClassGetRandomNumber
//DONE - ClassRequestCard
//DONE - ClassSaveResult 

public class Launcher {

	// Constructor
	Launcher() {
		int runs = 100;

		System.out.println("---------------MAIN_1---------------");
		System.out.println("Launcher - getting user input...");
		//ClassGetUserInput input = new ClassGetUserInput();
		int numc = 1;
		System.out.println("Launcher - user input = " + numc);
		
		while (runs > 0) {
			simulation(numc);
			runs--;
		}

		ClassSaveResult resultssimulation = new ClassSaveResult();
		System.out.println("Final Results are:" + resultssimulation.savedresults());

	}// Constructor

	public void simulation(int numc) {
		// => Start Simulation
		// getting input (number) from user
		// creating a new card deck
		System.out.println("---------------MAIN_2---------------");
		System.out.println("Launcher - creating new carddeck...");
		ClassCreateDeck deck = new ClassCreateDeck();
		ArrayList<Integer> cards = new ArrayList<Integer>(deck.createdeck(numc));
		System.out.println("Launcher - the created deck is = " + cards);
		// starts a new round
		System.out.println("---------------MAIN_3---------------");
		System.out.println("Launcher - starting new game...");
		ClassNewRound newround = new ClassNewRound();
		newround.newgame(cards);
		System.out.println("Launcher - finished loading new game");
		// Compare the player's and the dealer's card
		System.out.println("---------------MAIN_4---------------");
		System.out.println("Launcher - checking winrate for different actions...");
		ClassCheckCards check = new ClassCheckCards();
		float payment = (float) check.checkcards(cards);
		System.out.println("Launcher - finished comparing cards... the players avarage payment was = " + payment);
		System.out.println("---------------MAIN_5---------------");
		System.out.println("Launcher - saving results to list... result was = " + payment);
		ClassSaveResult save = new ClassSaveResult();
		save.saveresult(payment);
		System.out.println("Launcher - data has been saved" + "\nSimulation finished."
				+ "\n------------------------------------------------------------------------------------------");


	}

	public static void main(String[] args) {
		new Launcher();

	}// MAIN

}// Class
