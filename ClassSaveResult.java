package BlackjackSimulation;

import java.util.ArrayList;

public class ClassSaveResult {
	//Instance variables
	private static ArrayList<Float> savedresult = new ArrayList<Float>();
	
	//Constructor
	public ClassSaveResult() {
	}//Constructor
	
	public float calculateresult(int hitwin,int hitdraw,int hitlose) {
		System.out.println("ClassSaveRasult - calculation final result...");
		float finalresult = ((float) 2.0 * (float) hitwin + (float) 1.0 *  (float) hitdraw + (float) 0.0 *  (float) hitlose)/52;
		System.out.println("ClassSaveResult - retunring result with the value = " + finalresult);
		return finalresult;
	}
	
	public void saveresult(float payment) {
		System.out.println("ClassSaveResult - saving data...");
		ClassNewRound info = new ClassNewRound();
		float block = info.getfirstdealercard();
		float blockposition = info.getplayercardsvalue();
//		int position = block*18 + blockposition;
		savedresult.add(block);
		savedresult.add(blockposition);
		savedresult.add(payment);
	}

	public ArrayList<Float> savedresults(){
		System.out.println("ClassSaveResult - returning saved results = " + savedresult);
		return savedresult;
	}
	
}//Class
