package BlackjackSimulation;

import java.util.ArrayList;

public class ClassCheckCards {
	//Instance variables
	private static double result = 0.0;
	
	private static int hitwin = 0;
	private static int hitdraw = 0;
	private static int hitlose = 0;
	
	//Constructor
	public ClassCheckCards() {
	}//Constructor{
	
	public void resetClassCheckCards() {
		System.out.println("ClassCheckCards - reseting ClassCheckCards...");
		result = 0.0;
		hitwin = 0;
		hitdraw = 0;
		hitlose = 0;
		System.out.println("ClassCheckCards - reseting ClassCheckCards finished.");
	}
	
	public double checkcards(ArrayList<Integer>cards) {
		//overall check of the current situation of the dealers and players cards
		//checks for Blackjack further information at the switch
		System.out.println("ClassCheckCards - running a comparison of the cards...");
		switch (checkforplayerblackjack() + checkfordealerblackjack()) {
		case 1:
			//the player has a blackjack (the player's win rate = 100%)
			System.out.println("ClassCheckCards - the result is a player wins... the player has a blackjack");
			result = 2.5;
			break;	
		case 2:
			//the dealer has a blackjack (the player's win rate = 0%)
			System.out.println("ClassCheckCards - the result is a dealer wins... the dealer has a blackjack");
			result = 0.0 ;
			break;
		case 3:
			//both (the player and the dealer)  have a blackjack
			System.out.println("ClassCheckCards - the result is a draw... both have a blackjack");
			result = 1.0;
			break;
		case 0:
			//no one has a blackjack
			System.out.println("ClassCheckCards - no one has a blackjack... cards comparison continues...");
			ClassNewRound info = new ClassNewRound();
			ClassCreateDeck deckinfo = new ClassCreateDeck();
			int cardstocheck = deckinfo.getcardsinthedeck();
			while (cardstocheck > 0) {
				System.out.println("ClassCheckCards - getting played a third card...");
				int newplayercardsvalue = info.getplayercardsvalue() + cards.get(cardstocheck);
				comparecards(newplayercardsvalue, info.getdealercardsvalue());
				cardstocheck--;
			}
			System.out.println("ClassCheckCards - calculation possibilities finished: " + "win = " + hitwin + " draw = " + hitdraw + " lose = " + hitlose);
			ClassSaveResult calculate = new ClassSaveResult();
			result = calculate.calculateresult(hitwin, hitdraw, hitlose);
			break;
		}
		System.out.println("ClassCheckCards - returning result = " + result);
		return result;
	}
	public int checkforplayerblackjack() {
		System.out.println("ClassCheckCards - checking for the player's hand for a blackjack...");
		ClassNewRound info = new ClassNewRound();
		//does the player have a Blackjack?
		if (info.getplayercardsvalue() == 21) {
			System.out.println("ClassCheckCards - player has a blackjack - returning 1");
			return 1;
		} else {
			System.out.println("ClassCheckCards - player does not have  a blackjack - returning 0");
			return 0;
		}	
	}
	
	public int checkfordealerblackjack() {
		System.out.println("ClassCheckCards - checking for the dealer's  hand for a blackjack...");
		ClassNewRound info = new ClassNewRound();
		//does the dealer have a Blackjack and only two cards? 
		if (info.getturndealer() == 2 && info.getdealercardsvalue() == 21) {
			System.out.println("ClassCheckCards - dealer has a blackjack - returning 1");
			return 2;
		} else {
			System.out.println("ClassCheckCards - dealer does not have  a blackjack - returning 0");
			return 0;
		}	
	}
	
	public ArrayList<Integer> hitcount(){
		ArrayList<Integer> hitcount = new ArrayList<Integer>();
		hitcount.add(hitwin);
		hitcount.add(hitdraw);
		hitcount.add(hitlose);
		System.out.println("ClassCheckCards - returning hitcount with <Win, Push, Lose> = " + hitcount);
		return hitcount;
	}
	
	public void comparecards(int newplayercardsvalue, int dealercardsvalue ) {
		System.out.println("ClassCheckCards - comparing player's and dealer's cards");
		//did the player bust?
		if (newplayercardsvalue > 21) {
			hitlose++;
			System.out.println("ClassCheckCards - player busted, lose"); 
			System.out.println("ClassCheckCards - check finished ----------");
			//if not, did the dealer bust?
		} else if (dealercardsvalue > 21) {
			hitwin++;
			System.out.println("ClassCheckCards - dealer busted, win");
			System.out.println("ClassCheckCards - check finished ----------");
			//if not, is it a draw?
		} else if (dealercardsvalue == newplayercardsvalue) {
			hitdraw++;
			System.out.println("ClassCheckCards - same value of cards, draw");
			System.out.println("ClassCheckCards - check finished ----------");
			//does the player have better cards?
		} else if (newplayercardsvalue > dealercardsvalue) {
			hitwin++;
			System.out.println("ClassCheckCards - player has higher card's value - win");
			System.out.println("ClassCheckCards - check finished ----------");
			//if not, the player loses
		} else {
			hitlose++;
			System.out.println("ClassCheckCards - dealer has higher card's value - lose");
			System.out.println("ClassCheckCards - check finished ----------");
		}
	}
	

}//Class
