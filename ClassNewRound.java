package BlackjackSimulation;

import java.util.ArrayList;

public class ClassNewRound {
	private static int  playercardsvalue;		//value of the cards of the player
	private static int dealercardsvalue;		//value of the cards of the dealer
	private static int newcardplayer;		//value of the latest drawn card for the player
	private static int newcarddealer;		//value of the latest drawn card for the dealer
	private static int playerblackjack;		//amount of drawn cards for the player
	private static int turndealer;		//amount of drawn cards for the dealer
	private static int firstdealercard;		//first card the dealer got dealt, the card the player can "see"
	
	//Constructor
	public ClassNewRound() {
	}//Constructor
	
	public void resetClassNewRound() {
		System.out.println("ClassNewRound - reseting ClassNewRound...");
		playercardsvalue = 0;
		dealercardsvalue = 0;
		newcardplayer = 0;
		newcarddealer = 0;
		playerblackjack = 0;
		turndealer = 0;
		firstdealercard = 0;
		System.out.println("ClassNewRound - reseting ClassNewRound finished.");
	}
	
	//starts a new game by giving the dealer and the player each 2 cards
	public void newgame(ArrayList<Integer> cards) {
		//Variables
		ArrayList<Integer> playercards = new ArrayList<Integer>();		//list with the cards of the player
		ArrayList<Integer> dealercards = new ArrayList<Integer>();		//list with the cards of the dealer
		
		//get Class
		ClassCheckCards check = new ClassCheckCards();
		
		//Reset
		check.resetClassCheckCards();
		resetClassNewRound();
		
		//Create 
		ClassCreateDeck card = new ClassCreateDeck();
		
		//gives the player cards
		System.out.println("ClassNewRound - giving cards to the player...");
		newcardplayer = card.getcard(cards);
		playercards.add(newcardplayer);
		playercardsvalue = playercardsvalue + newcardplayer;
		newcardplayer = card.getcard(cards);
		playercards.add(newcardplayer);
		playercardsvalue = playercardsvalue + newcardplayer;
		if (playercardsvalue == 21) {
			playerblackjack = 1;
		}
		System.out.println("ClassNewRound - playercardsvalue after starting new round is = " + playercardsvalue);
		System.out.println("ClassNewRound - playercards list = " + playercards);
		
		//giving the dealer cards
		System.out.println("ClassNewRound - giving cards to the dealer...");
		while (dealercardsvalue < 17) {
			newcarddealer = card.getcard(cards);
			if (turndealer == 0) {
				firstdealercard = newcarddealer;
			}
			dealercardsvalue = dealercardsvalue + newcarddealer;
			dealercards.add(newcarddealer);
			turndealer++;
		}
		System.out.println("ClassNewRound - dealercardsvalue after giving cards is = " + dealercardsvalue);
		System.out.println("ClassNewRound -dealercards  list = " + dealercards); 
		
	}
	
	public  int getplayercardsvalue() {
		System.out.println("ClassNewRound - returning playercardsvalue..." + " playercardsvalue is = " + playercardsvalue);
		return playercardsvalue;
	}
	
	public int getdealercardsvalue() {
		System.out.println("ClassNewRound - returning dealercardsvalue..." + " dealercardsvalue is = " + dealercardsvalue);
		return dealercardsvalue;
	}
	
	public int getnewcardplayer() {
		System.out.println("ClassNewRound - returning newcardplayer..." + " newcardplayer is = " + newcardplayer);
		return newcardplayer;
	}
	
	public int getplayerblackjack() {
		System.out.println("ClassNewRound - returning playerblackjack..." + " playerblackjack is = " + playerblackjack);
		return playerblackjack;
	}
	
	public int getturndealer() {
		System.out.println("ClassNewRound - returning turndealer..." + " turndealer is = " + turndealer);
		return turndealer;
	}
	
	public int getfirstdealercard() {
		System.out.println("ClassNewRound - returning firstdealercard..." + " firstdealercard is = " + firstdealercard);
		return firstdealercard;
	}

}//Class
